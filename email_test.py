from flask import Flask, render_template, url_for
from premailer import Premailer
app = Flask(__name__)

doctype = """<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
"""

def render_email(name):
    locals = {
        'title': 'Titulo',
    }

    return doctype + Premailer(
        render_template(name, **locals),
        strip_important=False
    ).transform()

@app.route('/')
def get_email():
    # premailer rips off the doctype
    return render_email('mail.html')
